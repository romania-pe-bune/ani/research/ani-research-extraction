# how to use this

# first login to AZURE from a cmd with Azure CLI
az login

# you should have access to the subscription IT PeBune - Free Plan
az account show --subscription "IT PeBune - Free Plan"


# verify the subsciption contains a resource group named ocrdocs
az group list --subscription "IT PeBune - Free Plan"


# verify the deployment files are valid
az deployment group validate --parameters azuredeploy.parameters.json --resource-group ocrdocs --template-file azuredeploy.json --subscription "IT PeBune - Free Plan"


# deploy the resources
az deployment group create --parameters azuredeploy.parameters.json --resource-group ocrdocs --template-file azuredeploy.json --subscription "IT PeBune - Free Plan"


# deploy storage account and cognitive services for OCR
az deployment group create --parameters initialTemplates/01storageAccount.parameters.json --resource-group ocrdocs --template-file initialTemplates/01storageAccount.json --subscription "IT PeBune - Free Plan"

az deployment group create --parameters initialTemplates/02computerVision.parameters.json --resource-group ocrdocs --template-file initialTemplates/02computerVision.json --subscription "IT PeBune - Free Plan"

az deployment group create --parameters initialTemplates/03formService.parameters.json --resource-group ocrdocs --template-file initialTemplates/03formService.json --subscription "IT PeBune - Free Plan"

az deployment group create --parameters initialTemplates/04function.parameters.json --resource-group ocrdocs --template-file initialTemplates/04function.json --subscription "IT PeBune - Free Plan"

